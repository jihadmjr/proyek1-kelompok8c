from django.db import models

# Create your models here.
class Profile(models.Model):
	name = models.CharField(max_length=250)
	birthday = models.CharField(max_length=200)
	gender = models.CharField(max_length=200)
	expertise = models.TextField(max_length=200)
	description = models.CharField(max_length=200)
	email = models.CharField(max_length=200)
