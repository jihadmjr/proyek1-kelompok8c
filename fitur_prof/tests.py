from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

class ProfUnitTest(TestCase):
	def test_profile_url_is_exist(self):
		response = Client().get('/prof/')
		self.assertEqual(response.status_code, 200)

	def test_profile_using_index_func(self):
		found = resolve('/prof/')
		self.assertEqual(found.func, index)
