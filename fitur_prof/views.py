from django.shortcuts import render
from django.http import HttpResponseRedirect
from datetime import datetime, date
from fitur_prof.models import Profile

# Create your views here.
name = 'Linked-Us' #TODO implement your name here
birthdate = "5 October 2017" #TODO implement your birthday
# birthdate = birth_date.strftime('%d %B %Y')
gender = 'Unknown' #TODO implement your gender
email = 'admin@linkedus.com' #TODO implement your email
desc_profile = "We Help You To Link Each Other"
#TODO implement your expertise minimal 3
expert = ["Django", "Python", "HTML", "CSS"]
def index(request):
    html = 'prof.html'
    profil = Profile(name = name, birthday = birthdate, gender= gender, email = email, description=desc_profile, expertise=expert)
    response = {'name' : profil.name, 'birthday' : profil.birthday, 'gender': profil.gender, 'expertise': profil.expertise,
                'description' : profil.description, 'email': profil.email}
    return render(request, html, response)
