Proyek 1 PPW 2017/2018

[![pipeline status](https://gitlab.com/jihadmjr/proyek1-kelompok8c/badges/master/pipeline.svg)](https://gitlab.com/jihadmjr/proyek1-kelompok8c/commits/master)

[![coverage report](https://gitlab.com/jihadmjr/proyek1-kelompok8c/badges/master/coverage.svg)](https://gitlab.com/jihadmjr/proyek1-kelompok8c/commits/master)



Anggota Kelompok 8C:

- Muhammad Jihad Rinaldi  - 1606883392
- Jihan Amalia Irfani     - 1606837846
- Jeffry Aldi Permana     - 1606876973
- Ahmad Nur Barraza       - 1606875913

Link Heroku:
http://linkedus.herokuapp.com/
