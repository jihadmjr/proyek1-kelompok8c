from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

# Create your tests here.
class StatsUnitTest(TestCase):
    def test_stats_url_is_exist(self):
        response = Client().get('/stats/')
        self.assertEqual(response.status_code, 200)

    def test_stats_using_index_function(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, index)

    def test_navbar_is_exist(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<nav class="navbar navbar-inverse navbar-fixed-top">', html_response)        
        self.assertIn('&copy', html_response)
