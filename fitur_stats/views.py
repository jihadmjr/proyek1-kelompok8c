from django.shortcuts import render
from django.db import models
from fitur_add_friend.models import Friend
from fitur_update_status.models import Status

response = {'author': "Linked-Us"}

# Create your views here.
def index(request):
    html = 'stats/stats.html'
    counting_all_friends = Friend.objects.all().count()
    counting_all_feeds = Status.objects.all().count()
    latest_status = Status.objects.last()
    response['friends'] = counting_all_friends
    response['feeds'] = counting_all_feeds
    response['status'] =latest_status
    return render(request, html, response)