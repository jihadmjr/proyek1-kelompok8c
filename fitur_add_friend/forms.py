from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required' : 'Tolong isi input ini',
        'invalid' : 'Isi input ini dengan url',
    }
    attrs = {
        'class' : 'form-control'
    }

    name = forms.CharField(label='Nama', required=True, max_length=30, widget=forms.TextInput(attrs=attrs))
    URL = forms.URLField(required=True, widget=forms.URLInput(attrs=attrs))
