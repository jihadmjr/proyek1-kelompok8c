from django.test import Client
from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from .models import Friend
from .forms import Friend_Form
from .views import index, add_friend

class addFriendUnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/add_friend/')
		self.assertEqual(response.status_code, 200)
	def test_add_friend_using_index(self):
		found = resolve('/add_friend/')
		self.assertEqual(found.func,index)
	def test_add_friend_create_new_friend(self):
		new_activity = Friend.objects.create(name='baraja ganteng',URL='http://google.com') #buat baru
		count_friend = Friend.objects.all().count() #hitung semua objek
		self.assertEqual(count_friend,1) #kalo jadi, jumlah friend == 1
	def test_str_equal_to_name(self):
		new_activity = Friend.objects.create(name='baraja ganteng',URL='http://google.com')
		self.assertEqual(new_activity.name,new_activity.__str__())
	def test_add_friend_get_success(self):
		response = self.client.post('/add_friend/add_friend/', {'name': 'Linked-uS', 'URL': 'http://Linked-uS.herokuapp.com'})
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/add_friend')
		new_response = self.client.get('/add_friend/')
		html_response = new_response.content.decode('utf8')
		self.assertIn('Linked-uS', html_response)
	def test_add_friend_post_fail(self):
		dump = {'name':'', 'URL':''}
		response = self.client.get('/add_friend/add_friend/', dump)
		self.assertEqual(response.status_code, 302)
