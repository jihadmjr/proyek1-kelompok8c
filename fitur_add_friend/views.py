from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend
from datetime import datetime

# Create your views here.

def index(request):
    response = {'author': "Linked-uS"}
    response['friend_form'] = Friend_Form
    friend_dict = Friend.objects.all().values()
    response['friend_dict'] = convert_queryset_into_json(friend_dict)
    html = 'add_friend.html'
    return render(request, html, response)

def add_friend(request):
    form = Friend_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        friend = Friend(name=request.POST['name'], URL=request.POST['URL'])
        friend.save()
        return HttpResponseRedirect('/add_friend')
    else:
        return HttpResponseRedirect('/add_friend')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val
