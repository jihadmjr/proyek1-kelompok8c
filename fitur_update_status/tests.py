from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Status
from .views import index, update_status
from .forms import Status_Form

# Create your tests here.
class UpdateStatusUnitTest(TestCase):
    def test_stats_url_is_exist(self):
        response = Client().get('/update-status/')
        self.assertEqual(response.status_code, 200)

    def test_stats_using_index_function(self):
        found = resolve('/update-status/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_status = Status.objects.create(status='hari ini saya merasa senang')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="status-form-textarea', form.as_p())
        self.assertIn('id="id_status', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
    def test_update_status_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update-status/update_status', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_update_status_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/update-status/update_status', {'status': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)