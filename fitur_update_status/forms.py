from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi status disini',
    }
    status_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'status-form-textarea',
        'placeholder':'How Are you today?'
    }

    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=status_attrs))